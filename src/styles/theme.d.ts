import 'styled-components';
import '@mui/material/styles';

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      background: string,
      primary: string,
      secondary: string,
    };
  }
}

declare module '@mui/material/styles' {
  interface DefaultTheme {
    colors: {
      background: string,
      primary: string,
      secondary: string,
    };
  }
}