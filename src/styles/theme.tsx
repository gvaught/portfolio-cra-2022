// theme.tsx
import { createTheme } from '@mui/material/styles';

import './styles.css';
import { PaletteMode } from '@mui/material';

interface PaletteColor {
  light?: string;
  main: string;
  dark?: string;
  contrastText?: string;
}

export const darkTheme = createTheme({
  palette: {
    mode: 'dark',
    text: {
      primary: '#fac9ce',
      secondary: '#736263',
    },
    info: {
      main: '#33a9ac',
    },
  },
});

export const lightTheme = createTheme({
  palette: {
    mode: 'light',
    text: {
      primary: '#736263',
      secondary: '#fac9ce',
    },
    info: {
      main: '#33a9ac',
    },
  },
});

export const getDesignTokens = (mode: PaletteMode) => ({
  palette: {
    mode,
    ...(mode === 'light'
      ? {
          background: {
            default: '#fac9ce',
            // paper: '',
          },
          text: {
            primary: '#736263',
            secondary: '#fac9ce',
          },
          info: {
            main: '#33a9ac',
          },
        }
      : {
          text: {
            primary: '#fac9ce',
            secondary: '#736263',
          },
          info: {
            main: '#33a9ac',
          },
        }),
  },
  typography: {
    fontFamily: [
      'Recursive',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
});