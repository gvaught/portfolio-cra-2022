import { useTheme } from '@mui/material/styles';
import styled from 'styled-components';

export interface ButtonProps {
  text: string;
  href?: string;
  download?: boolean;
}

const StyledButton = styled.a`
  background-color: ${props => props.theme.palette.mode === 'dark' ? 'var(--coral)' : 'var(--beige)'};
  border: none;
  color: ${props => props.theme.palette.text.secondary};
  padding: 8px 16px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-family: ${props => props.theme.typography.fontFamily};
  font-size: ${props => props.theme.typography.caption};
  box-shadow: 2px 2px ${props => props.theme.palette.mode === 'dark' ? 'var(--beige)' : 'var(--dark)'};
  transition: all 250ms ease-in-out;
  cursor: pointer;
  &:hover {
    background-color: ${props => props.theme.palette.mode === 'dark' ? 'var(--beige)' : 'var(--coral)'};
    color: ${props => props.theme.palette.text.primary};
    box-shadow: 4px 4px ${props => props.theme.palette.mode === 'dark' ? 'var(--coral)' : 'var(--beige)'};
  }
`

export const Button = (props: ButtonProps) => {
  const theme = useTheme();

  return (
    <StyledButton
      theme={theme}
      href={props.href || ''}
      download={props.download}
    >
      {props.text}
    </StyledButton>
  )
}