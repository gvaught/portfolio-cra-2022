import { Box, Typography } from "@mui/material";
import styled from "styled-components";
import { Button } from "./Button";

const Container = styled.section`
  max-width: 768px;
  margin: 0 auto;
  padding: 1rem;
  h3 {
    font-variation-settings: 'MONO' 1.0;
    font-size: 1.5rem;
    font-weight: 400;
  }
  h3.subheading {
    font-size: 2rem;
    text-align: center;
  }
  h1.main-heading {
    font-variation-settings: 'CASL' 1.0;
    font-size: 6rem;
    font-weight: 800;
    margin: 1rem auto;
  }
`

export const About = () => {
  return (
    <>
      <Container className="about">
        <Typography
          variant="h3"
          color="text.primary"
        >
          Hello world! I'm
        </Typography>
        <Typography
          variant="h1"
          color="text.primary"
          className="main-heading"
        >
          Gavin Vaught
        </Typography>
        <Typography
          variant="h3"
          color="text.primary"
          className="subheading"
        >
          A Web Developer.
        </Typography>
        <br />
        <Typography variant="body1">
          Adipisicing sit fugit ullam unde aliquid sequi Facilis soluta facilis perspiciatis corporis nulla aspernatur. Autem eligendi rerum delectus modi quisquam? Illo ut quasi nemo ipsa cumque perspiciatis! Maiores minima consectetur.
        </Typography>
        <br/>
        <Box sx={{display: 'flex', justifyContent: 'center', p: 3}}>
          <Button text="resume" href={require("./../assets/Resume-Gavin-Vaught.pdf")} download/>
        </Box>
      </Container>
    </>
  )
}