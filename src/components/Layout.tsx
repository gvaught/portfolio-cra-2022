import { Navigation } from './Navigation';
import { About } from './About';
import { Projects } from './Projects';
import { Contact } from './Contact';
import { Box } from '@mui/material';

export const Layout = () => {
  return (
    <>
      <About />
      <Projects />
      <Contact />
    </>
  )
}